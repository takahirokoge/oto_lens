clear
%条件設定
lx=0.2;%x方向の膜の大きさ(m)
ly=0.4;%y方向の膜の大きさ(m)
lt=0.01;%計算時間(s)
dx=1e-3;%x方向の距離刻み(m)
dy=1e-3;%y方向の距離刻み(m)
dt=1e-5;%時間刻み(s)
c=70;%音速(m/s)

%条件から定数等を決定
nx=round(lx/dx)+1;%x方向の配列の数
ny=round(ly/dy)+1;%y方向の配列の数
nt=round(lt/dt)+1;%t方向の配列の数
Kx=(c*dt/dx)^2;%差分計算の時に用いる係数
KX=zeros(nx,ny);
KX(:,:)=Kx;
Ky=(c*dt/dy)^2;%差分計算の時に用いる係数
KY=zeros(nx,ny);
KY(:,:)=Ky;
p=zeros(nx,ny,nt);%音圧値p(x,y,t)
p_p=zeros(nx,ny);%過去の値
n_p=zeros(nx,ny);%現在の値
f_p=zeros(nx,ny);%未来の値
ux_n=zeros(nx,ny);%x方向の粒子速度
uy_n=zeros(nx,ny);%y方向の粒子速度
ux_f=zeros(nx,ny);%x方向の粒子速度
uy_f=zeros(nx,ny);%y方向の粒子速度
x_1=1:nx-2; %x-1のベクトルを生成
x0=2:nx-1; %xの（以下同文）
x1=3:nx; %x+1
y_1=1:ny-2;%y-1
y0=2:ny-1;%y
y1=3:ny;%y+1

map=zeros(nx,ny);%エネルギー積分


%%%%%%%%%%% Initial condition %%%%%%%%%%%%%%     
[xn yn]=meshgrid(-(nx-1)/2:(nx-1)/2,-(ny-1)/2:(ny-1)/2);
w=nx/150;%ガウス分布のパルス幅（分散）                           
shift= 0;%ガウス分布の中心(平均値)
%p(160,:,1)=exp(-((xn.^2+(yn-50).^2).^0.5 - shift).^2/(2*w^2));%ガウス分布型の初期値を与える

for i=1:201
p(i,:,1)=exp(-((((1:ny)-30).^2).^0.5 - shift).^2/(10*w^2));%ガウス分布型の線音源の初期値を与える
end

p(:,:,2)=p(:,:,1);%%初期条件から静かに離す
p_p=p(:,:,1);
n_p=p(:,:,2);

R=2.3; %レンズの弱さ（強さの逆数） 2.3以下で発散しそう。
l=exp(((1:nx)-round(nx/2)).^2/(R*50^2)); %レンズ状の$1/\rho$の分布
for i=1:round(nx/10)
   l((10*i-9):(10*i))= mean(l((10*i-9):(10*i)));
end

%rhoの分布を作る
r=zeros(nx,ny);
r(:,:)=1;
%Wallを作る
for i=1:30
    r(:,50+i)=l;
end


%kappaの分布を作る
k=zeros(nx,ny);
k(:,:)=10;
%PML風の吸収条件
for i=1:nx
    k(i,ny-20+1:ny)=10*(1.5.^(1:+1:20));
    k(i,1:20)=10*(1.5.^(20:-1:1));
end
for i=1:ny
    k(1:20,i)=10*(1.5.^(20:-1:1));
    k(nx-20+1:+1:nx,i)=10*(1.5.^(1:+1:20));
end


%%%%%%%%%%%%%% FDTD %%%%%%%%%%%%%%%
for t=2:nt-1          
    ux_f(x0,y0)=ux_n(x0,y0)-r(x0,y0).*(f_p(x0,y0)-f_p(x_1,y0))./k(x0,y0);
    uy_f(x0,y0)=uy_n(x0,y0)-r(x0,y0).*(f_p(x0,y0)-f_p(x0,y_1))./k(x0,y0);
    f_p(x0,y0)=n_p(x0,y0)-(ux_f(x1,y0)-ux_f(x0,y0)+uy_f(x0,y1)-uy_f(x0,y0));    
    p_p=n_p;
    n_p=f_p;
    f_p(2,:)=f_p(1,:);
    ux_n=ux_f;
    uy_n=uy_f;
    p(:,:,t+1)=f_p;%uy_f;%
    map=map+abs(f_p);
end

%%%%%%%% Animation %%%%%%%%%%%%%%%%%
for t=1:nt
    subplot(211);
    mesh(p(:,:,t));
    axis([20 ny-20 20 nx-20 -1 1]);
    title("p(t)");
    subplot(212);
    mesh(map(20:nx-20,20:ny-20)) %エネルギー積分を表示
    axis([20 ny-20 20 nx-20 -1 1]);
    M(t)=getframe;
end

%figure(2);