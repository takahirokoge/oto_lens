clear
%条件設定
lx=0.2;%x方向の膜の大きさ(m)
ly=0.4;%y方向の膜の大きさ(m)
lt=0.00045;%計算時間(s)
dx=1e-3;%x方向の距離刻み(m)
dy=1e-3;%y方向の距離刻み(m)
dt=1e-7;%時間刻み(s)
c=70;%音速(m/s)
nu_p=10;%定在波の周波数[Hz]

%条件から定数等を決定
nx=round(lx/dx)+1;%x方向の配列の数
ny=round(ly/dy)+1;%y方向の配列の数
nt=round(lt/dt)+1;%t方向の配列の数
Kx=(c*dt/dx)^2;%差分計算の時に用いる係数
KX=zeros(nx,ny);
KX(:,:)=Kx;
Ky=(c*dt/dy)^2;%差分計算の時に用いる係数
KY=zeros(nx,ny);
KY(:,:)=Ky;
p=zeros(nx,ny,nt);%音圧値p(x,y,t)
p_p=zeros(nx,ny);%過去の値
n_p=zeros(nx,ny);%現在の値
f_p=zeros(nx,ny);%未来の値
ux_n=zeros(nx,ny);%x方向の粒子速度
uy_n=zeros(nx,ny);%y方向の粒子速度
ux_f=zeros(nx,ny);%x方向の粒子速度
uy_f=zeros(nx,ny);%y方向の粒子速度
x_1=1:nx-2; %x-1のベクトルを生成
x0=2:nx-1; %xの（以下同文）
x1=3:nx; %x+1
y_1=1:ny-2;%y-1
y0=2:ny-1;%y
y1=3:ny;%y+1

r=zeros(nx,ny,nt);%密度分布
map=zeros(nx,ny);%エネルギー積分

%%%%%%%%%%% Initial condition %%%%%%%%%%%%%%     
[xn yn]=meshgrid(-(nx-1)/2:(nx-1)/2,-(ny-1)/2:(ny-1)/2);
w=nx/150;%ガウス分布のパルス幅（分散）                           
shift= 0;%ガウス分布の中心(平均値)
%p(160,:,1)=exp(-((xn.^2+(yn-50).^2).^0.5 - shift).^2/(2*w^2));%ガウス分布型の初期値を与える

for i=1:201
p(i,:,1)=0.01*exp(-((((1:ny)-30).^2).^0.5 - shift).^2/(10*w^2));%ガウス分布型の線音源の初期値を与える
end

p(:,:,2)=p(:,:,1);%%初期条件から静かに離す
p_p=p(:,:,1);
n_p=p(:,:,2);

w=400; %レンズの広がり（弱さ）。
l=exp(-((1:nx)-round(nx/2)).^2/(w^2)); %レンズ状の$1/\rho$の分布
for i=1:round(nx/10)
   l((10*i-9):(10*i))= mean(l((10*i-9):(10*i)));
end

l=l/l(1);%縁の強さで規格化。
l(l<1)=1;

%rhoの分布を作る
r_a=zeros(nx,ny);
r_a(:,:)=1.2; % ← 物理的には 1.2 [kg/m^-3] 位が望ましい。
%Wallを作る
for i=1:30
    r_a(:,50+i)=r_a(:,50+i).*l';
end

%kappaの分布を作る
k=zeros(nx,ny);
k(:,:)=1.4e-1;%5; ← 物理的には 1.4e5[Pa] 位が望ましい。ちなみに，音速 c = sqrt(k/r) ~ 340[m/s]
%%PML風の吸収条件
eta=1.1; %吸収度合い
wa=30;%壁の厚さ
for i=1:nx
    k(i,ny-wa+1:ny)=k(i,ny-wa+1:ny).*(eta.^(1:+1:wa));
    k(i,1:wa)=k(i,1:wa).*(eta.^(wa:-1:1));
end
for i=1:ny
    k(1:wa,i)=k(1:wa,i).*(eta.^(wa:-1:1))';
    k(nx-wa+1:+1:nx,i)=k(nx-wa+1:+1:nx,i).*(eta.^(1:+1:wa))';
end
k(k>0.5)=0.5;

%%%%%%%%%%%%%% FDTD %%%%%%%%%%%%%%%
for t=2:nt-1
    r(:,:,t)=r_a;
    %r(95:105,51:81,t)=r(95:105,51:81,t)+0.5*sin(pi*t*nu_p/180)*r_a(95:105,51:81); %時間変化する密度分布
    ux_f(x0,y0)=ux_n(x0,y0)-(f_p(x0,y0)-f_p(x_1,y0))./r(x0,y0,t);
    uy_f(x0,y0)=uy_n(x0,y0)-(f_p(x0,y0)-f_p(x0,y_1))./r(x0,y0,t);
    f_p(x0,y0)=n_p(x0,y0)-k(x0,y0).*(ux_f(x1,y0)-ux_f(x0,y0)+uy_f(x0,y1)-uy_f(x0,y0));    
    p_p=n_p;
    n_p=f_p;
    f_p(2,:)=f_p(1,:);
    ux_n=ux_f;
    uy_n=uy_f;
    p(:,:,t+1)=f_p;%uy_f;%
    map=map+abs(f_p);
end

%%%%%%%% Animation %%%%%%%%%%%%%%%%%
for t=1:100:nt
    figure(1)
    subplot(211);
    mesh(p(:,:,t));
    axis([wa ny-wa wa nx-wa -0.01 0.01]);
    title("p(t)");
    subplot(212);
    mesh(r(:,:,t)) %エネルギー積分を表示
    axis([wa ny-wa wa nx-wa 1 1.5]);
    M(t)=getframe;
end

figure(2);
mesh(map(20:nx-20,20:ny-20));