import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


def nd(s, e, d=1):
    return np.array(range(s, e, d))


def set_condition():
    '''
    初期条件を設定
    :return:
    '''
    lx = 0.2      # x方向の膜の大きさ(m)
    ly = 0.4      # y方向の膜の大きさ(m)
    lt = 0.01     # 計算時間(s)
    dx = 0.001     # x方向の距離刻み(m)
    dy = 0.001     # y方向の距離刻み(m)
    dt = 0.00001     # 時間刻み(s)
    c = 70    # 音速(m/s)

    return lx, ly, lt, dx, dy, dt, c


def get_constances():
    '''
    初期条件から算出される定数の演算
    :return:
    '''
    lx, ly, lt, dx, dy, dt, c = set_condition()
    nx = round(lx/dx)+1         # x方向の配列の数
    ny = round(ly/dy)+1         # y方向の配列の数
    nt = round(lt/dt)+1         # t方向の配列の数
    Kx = (c*dt/dx)**2.0         # 差分計算の時に用いる係数
    Ky = (c*dt/dy)**2.0         # 差分計算の時に用いる係数
    p = np.zeros((nx, ny, nt))  # 音圧値p(x,y,t)
    p_p = np.zeros((nx, ny))    # 過去の値
    n_p = np.zeros((nx, ny))    # 現在の値
    f_p = np.zeros((nx, ny))    # 未来の値
    ux_n = np.zeros(nx, ny)           # x方向の粒子速度
    uy_n = np.zeros(nx, ny)           # y方向の粒子速度
    ux_f = np.zeros(nx, ny)           # x方向の粒子速度
    uy_f = np.zeros(nx, ny)           # y方向の粒子速度
    x_1 = nd(1, nx-2)  # x-1のベクトルを生成
    x0 = nd(2, nx-1)   # xの（以下同文）
    x1 = nd(3, nx)     # x+1
    y_1 = nd(1, ny-2)   # y-1
    y0 = nd(2, ny-1)   # y
    y1 = nd(3, ny)     # y+1
    r = np.zeros(nx, ny, nt)  # 密度分布
    map = np.zeros(nx, ny)

    return nx, ny, nt, Kx, Ky, p, p_p, n_p, f_p, ux_n, uy_n, ux_f, uy_f, x_1, x0, x1, y_1, y0, y1, r, map


def set_default(p, p_p, n_p, nx, ny):
    a = nd(int(-(nx-1)/2), int((nx-1)/2)+1)
    b = nd(int(-(ny-1)/2), int((ny-1)/2)+1)
    xn, yn = np.meshgrid(a, b)
    w = nx/20    # ガウス分布のパルス幅（分散）
    shift = 0    # ガウス分布の中心(平均値)
    p[:, :, 1] = np.exp(-((xn**2+yn**2)**0.5 - shift)**2/(2*w**2))    # ガウス分布型の初期値を与える
    p[:, :, 2] = p[:, :, 1]    # 初期条件から静かに離す
    p_p = p[:, :, 1]
    n_p = p[:, :, 2]

    for i in range(201):
        p[i, :, 1] = np.exp(np.power(-(np.power(np.power((nd(1, ny) - 30), 2), 0.5) - shift), 2) / (10 * w ** 2))

    w = 400
    l = np.exp(np.power(nd(1, nx)-round(nx/2), 2)/(w ** 2))

    for i in range(1, nx/10):
        l[10*i-9: 10*i] = np.mean(l[10*i-9: 10*i])

    l = l/l[0]

    # rhoの分布を作成
    r_a = np.zeros(nx, ny)
    r_a[:, :] = 1.2
    # Wallを作る
    for i in range(1, 30):
        r_a[:, 50 + i] = np.power(r_a[:, 50 + i], l).T

    # kappaの分布を作る
    k = np.zeros(nx, ny)
    k[:, :] = 1.4e-1  # 5; ← 物理的には 1.4e5[Pa] 位が望ましい。ちなみに，音速 c = sqrt(k/r) ~ 340[m/s]
    # PML風の吸収条件
    eta = 1.1  # 吸収度合い
    wa = 30    # 壁の厚さ

    for i in range(1, nx):
        k[i, ny-wa+1:ny] = k[i, ny-wa+1:ny] * np.power(eta, nd(1, wa)).T
        k[i, 1:wa] = k[i, 1:wa] * np.power(eta, nd(wa, 1, -1))

    for i in range(1, ny):
        k[1:wa, i] = k[1:wa, i] * np.power(eta, nd(wa, 1, -1)).T
        k[nx-wa+1:+1:nx, i] = k[nx-wa+1:+1:nx, i] * np.power(eta, nd(1, wa)).T

    k[0.5:k] = 0.5

    return p, p_p, n_p, xn, yn, r_a, k


def FDTD(nx, ny, nt, Kx, Ky, p, p_p, n_p, f_p, ux_n, uy_n, ux_f, uy_f, x_1, x0, x1, y_1, y0, y1, map, r_a, k):
    for t in range(2, nt-1):
        r[:, :, t] = r_a
        # r(95:105,51:81,t)=r(95:105,51:81,t)+0.5*sin(pi*t*nu_p/180)*r_a(95:105,51:81)  #時間変化する密度分布
        ux_f[x0, y0] = ux_n[x0, y0] - (f_p[x0, y0] - f_p[x_1, y0]) / r[x0, y0, t]
        uy_f[x0, y0] = uy_n[x0, y0] - (f_p[x0, y0] - f_p[x0, y_1]) / r[x0, y0, t]
        f_p[x0, y0] = n_p[x0, y0] - k[x0, y0] * (ux_f[x1, y0] - ux_f[x0, y0] + uy_f[x0, y1] - uy_f[x0, y0])
        p_p = n_p
        n_p = f_p
        f_p[2, :] = f_p[1, :]
        ux_n = ux_f
        uy_n = uy_f
        p[:, :, t+1] = f_p
        map = map + abs(f_p)
    return p


if __name__ == '__main__':
    nx, ny, nt, Kx, Ky, p, p_p, n_p, f_p, ux_n, uy_n, ux_f, uy_f, x_1, x0, x1, y_1, y0, y1, r, map = get_constances()
    p, p_p, n_p, xn, yn, r_a, k = set_default(p, p_p, n_p, nx, ny)
    p = FDTD(nx, ny, nt, Kx, Ky, p, p_p, n_p, f_p, ux_n, uy_n, ux_f, uy_f, x_1, x0, x1, y_1, y0, y1, map, r_a, k)

    for t in range(1, nt):
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        surf = ax.plot_wireframe(xn, yn, p[:, :, t], cmap='bwr', linewidth=0)
        fig.colorbar(surf)
        ax.set_title("Surface Plot")
        ax.set_xlim(-20, 20)
        ax.set_ylim(-20, 20)
        ax.set_zlim(-1, 1)
        plt.show()
        plt.close()